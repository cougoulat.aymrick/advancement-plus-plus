import 'dotenv/config';
import App from './app';
// import validateEnv from './utils/validateEnv';

if (!process.env.PORT) {
  process.exit(1);
}
// validateEnv();

const app = new App();
app.listen();

export { app };
