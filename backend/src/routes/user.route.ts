import { Router } from 'express';
import UserController from '../controllers/user/user.controller';

const router = Router();
const userController = new UserController();

router.get('/test', userController.test);
// router.get('/', userController.getAll);
// router.post('/signup', userController.newUser);

export default router;